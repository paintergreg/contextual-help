package glp.digester.contextualHelp;


/**
 * Type Description Goes Here.
 * 
 * @author Greg Painter
 * @version $Id: $
 * 
 */
public class SystemDocModuleUrlMap {

	private String moduleCd;
	private String moduleName;
	private VtApimsModule vtApimsModule;

	public SystemDocModuleUrlMap() {
	}

	public String getModuleCd() {
		return moduleCd;
	}

	public void setModuleCd(String moduleCd) {
		this.moduleCd = moduleCd;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public VtApimsModule getVtApimsModule() {
		return vtApimsModule;
	}

	public void setVtApimsModule(VtApimsModule vtApimsModule) {
		this.vtApimsModule = vtApimsModule;
	}
}
