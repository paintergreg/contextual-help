package glp.digester.contextualHelp;

import java.util.ArrayList;
import java.util.List;

/**
 * Type Description Goes Here.
 * 
 * @author Greg Painter
 * @version $Id: $
 * 
 */
public class ContextualHelp {

	private List<SystemDocModuleUrlMap> sysDocUrlMap;

	public ContextualHelp() {
		sysDocUrlMap = new ArrayList<SystemDocModuleUrlMap>();
	}

	public void addSystemDocModuleUrlMap(SystemDocModuleUrlMap sysDocUrlMap) {
		this.sysDocUrlMap.add(sysDocUrlMap);
	}

	public List<SystemDocModuleUrlMap> getSysDocUrlMap() {
		return sysDocUrlMap;
	}

	public void setSysDocUrlMap(List<SystemDocModuleUrlMap> sysDocUrlMap) {
		this.sysDocUrlMap = sysDocUrlMap;
	}
}
