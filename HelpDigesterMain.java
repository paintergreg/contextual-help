package glp.digester.contextualHelp;

/**
 * Temporary class to test the contextual help.
 * 
 * @author Greg Painter
 * @version $Id: $
 * 
 */
public class HelpDigesterMain {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		HelpDigester helpDigester = new HelpDigester();
		ContextualHelp contextualHelp = helpDigester.execute();
		GenerateContextualHelp genHelp = new GenerateContextualHelp(contextualHelp);
		genHelp.execute();
	}

}
