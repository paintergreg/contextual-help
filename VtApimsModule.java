package glp.digester.contextualHelp;

import java.util.ArrayList;
import java.util.List;

/**
 * Type Description Goes Here.
 * 
 * @author Greg Painter
 * @version $Id: $
 * 
 */
public class VtApimsModule {

	private String description;
	private String statusCd;
	private String startDt;
	private String moduleName;
	private List<SystemDocumentContextHelp> sysDocContextHelp;

	public VtApimsModule() {
		sysDocContextHelp = new ArrayList<SystemDocumentContextHelp>();
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatusCd() {
		return statusCd;
	}

	public void setStatusCd(String statusCd) {
		this.statusCd = statusCd;
	}

	public String getStartDt() {
		return startDt;
	}

	public void setStartDt(String startDt) {
		this.startDt = startDt;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public List<SystemDocumentContextHelp> getSysDocContextHelp() {
		return sysDocContextHelp;
	}

	public void setSysDocContextHelp(List<SystemDocumentContextHelp> sysDocContextHelp) {
		this.sysDocContextHelp = sysDocContextHelp;
	}

	public void addSysDocContextHelp(SystemDocumentContextHelp sysDocContextHelp) {
		this.sysDocContextHelp.add(sysDocContextHelp);
	}
}
