package glp.digester.contextualHelp;

import java.text.MessageFormat;
import java.util.List;

/**
 * Type Description Goes Here.
 * 
 * @author Greg Painter
 * @version $Id: $
 * 
 */
public class GenerateContextualHelp {
	private final static String NEW_LINE = System.getProperty("line.separator");
	private final static String INS_MODULE = "INSERT INTO vt_apims_module (module_cd,description,status_cd,start_dt,module_name){0}     SELECT ''{1}'',''{2}'',''H'',SYSDATE,''apims'' FROM dual{0}      WHERE NOT EXISTS (SELECT 1 FROM vt_apims_module x WHERE x.module_cd = ''{1}''){0}        AND NOT EXISTS (SELECT 1 FROM system_doc_module_url_map x WHERE x.url_module_name = ''{2}'');";
	private final static String INS_MAP = "INSERT INTO system_doc_module_url_map (module_cd,url_module_name){0}     SELECT ''{1}'',''{2}'' FROM dual{0}      WHERE NOT EXISTS (SELECT 1 FROM system_doc_module_url_map x WHERE x.url_module_name = ''{2}'');";
	private final static String INS_HELP = "INSERT INTO system_document_context_help (module_cd,title,jsp_file_name,username){0}     SELECT module_cd,''{1}'',''{2}'',''{3}'' FROM system_doc_module_url_map{0}      WHERE url_module_name = ''{4}''{0}        AND NOT EXISTS (SELECT 1 FROM system_document_context_help x WHERE x.title = ''{1}'' AND x.jsp_file_name = ''{2}'');";
	private ContextualHelp contextualHelp = null;

	public GenerateContextualHelp() {

	}

	public GenerateContextualHelp(ContextualHelp contextHelp) {
		this.contextualHelp = contextHelp;
	}

	public void execute() {
		if (contextualHelp == null) {
			throw new IllegalStateException();
		}
		List<SystemDocModuleUrlMap> urlMap = contextualHelp.getSysDocUrlMap();
		for (SystemDocModuleUrlMap c : urlMap) {
			Object[] args = new Object[] { NEW_LINE, c.getModuleCd(), c.getModuleName() };
			System.out.println(MessageFormat.format(INS_MAP, args));

			VtApimsModule vt = c.getVtApimsModule();
			Object[] args1 = new Object[] { NEW_LINE, c.getModuleCd(), vt.getDescription() };
			System.out.println(MessageFormat.format(INS_MODULE, args1));

			List<SystemDocumentContextHelp> cHelp = vt.getSysDocContextHelp();
			for (SystemDocumentContextHelp s : cHelp) {
				Object[] args2 = new Object[] { NEW_LINE, s.getTitle(), s.getJspFileName(), s.getUserName(), c.getModuleName() };
				System.out.println(MessageFormat.format(INS_HELP, args2));
			}
			System.out.println("------------------------------------------");
		}
	}
}
