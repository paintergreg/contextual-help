package glp.digester.contextualHelp;

import java.io.File;

import org.apache.commons.digester.Digester;

/**
 * Type Description Goes Here.
 * 
 * @author Greg Painter
 * @version $Id: $
 * 
 */
public class HelpDigester {

	public HelpDigester() {

	}

	public ContextualHelp execute() {
		ContextualHelp contextualHelp = null;
		try {
			Digester digester = new Digester();
			digester.setValidating(false);

			digester.addObjectCreate("ContextualHelp", ContextualHelp.class);
			digester.addObjectCreate("ContextualHelp/SystemDocModuleUrlMap", SystemDocModuleUrlMap.class);
			digester.addBeanPropertySetter("ContextualHelp/SystemDocModuleUrlMap/moduleCd", "moduleCd");
			digester.addBeanPropertySetter("ContextualHelp/SystemDocModuleUrlMap/urlModuleName", "moduleName");
			digester.addSetNext("ContextualHelp/SystemDocModuleUrlMap", "addSystemDocModuleUrlMap");

			digester.addObjectCreate("ContextualHelp/SystemDocModuleUrlMap/VtApimsModule", VtApimsModule.class);
			digester.addBeanPropertySetter("ContextualHelp/SystemDocModuleUrlMap/VtApimsModule/description", "description");
			digester.addBeanPropertySetter("ContextualHelp/SystemDocModuleUrlMap/VtApimsModule/statusCd", "statusCd");
			digester.addBeanPropertySetter("ContextualHelp/SystemDocModuleUrlMap/VtApimsModule/startDt", "startDt");
			digester.addBeanPropertySetter("ContextualHelp/SystemDocModuleUrlMap/VtApimsModule/moduleName", "moduleName");
			digester.addSetNext("ContextualHelp/SystemDocModuleUrlMap/VtApimsModule", "setVtApimsModule");

			digester.addObjectCreate("ContextualHelp/SystemDocModuleUrlMap/VtApimsModule/SystemDocumentContextHelp", SystemDocumentContextHelp.class);
			digester.addBeanPropertySetter("ContextualHelp/SystemDocModuleUrlMap/VtApimsModule/SystemDocumentContextHelp/title", "title");
			digester.addBeanPropertySetter("ContextualHelp/SystemDocModuleUrlMap/VtApimsModule/SystemDocumentContextHelp/jspFileName", "jspFileName");
			digester.addBeanPropertySetter("ContextualHelp/SystemDocModuleUrlMap/VtApimsModule/SystemDocumentContextHelp/userName", "userName");
			digester.addSetNext("ContextualHelp/SystemDocModuleUrlMap/VtApimsModule/SystemDocumentContextHelp", "addSysDocContextHelp");

			File input = new File("./src/glp/digester/contextualHelp/help.xml");
			contextualHelp = (ContextualHelp) digester.parse(input);

		} catch (Exception exc) {
			exc.printStackTrace();
		}

		return contextualHelp;
	}
}
