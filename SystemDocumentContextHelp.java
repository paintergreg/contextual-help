package glp.digester.contextualHelp;

/**
 * Type Description Goes Here.
 * 
 * @author Greg Painter
 * @version $Id: $
 * 
 */
public class SystemDocumentContextHelp {
	private String title;
	private String jspFileName;
	private String userName;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getJspFileName() {
		return jspFileName;
	}

	public void setJspFileName(String jspFileName) {
		this.jspFileName = jspFileName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
}
